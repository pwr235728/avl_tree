#include "AvlTree.h"
#include <ctime>
#include <algorithm>


void PrintMenu() ;
void menu_insert(AvlTree<int>& tree);
void menu_remove(AvlTree<int>& tree);
void menu_draw(const AvlTree<int>& tree);
void menu_print(AvlTree<int>& tree);

void menu_generate(AvlTree<int>& tree)
{
	std::cout << "Generating keys: ";
	srand(static_cast<unsigned int>(time(nullptr)));
	for (int i = 0; i < 30; i++)   // Wype�niamy drzewo w�z�ami
	{
		const auto k = 1 + rand() % 50;
		std::cout << k << " | ";
		tree.Insert(k);
	}
}

int main()
{
	AvlTree<int> tree;



	std::string choise = "_";

	PrintMenu();
	do
	{
		std::cin >> choise;
		if (std::cin.eof()) continue;
		
		if (choise == "i") { menu_insert(tree); }
		else if (choise == "r") { menu_remove(tree); }
		else if (choise == "d") { menu_draw(tree); }
		else if (choise == "p") { menu_print(tree); }
		else if (choise == "q") { }
		else if (choise == "h") { PrintMenu(); }
		else if (choise == "g")
		{
			menu_generate(tree);
			std::cout << "\nGenerated tree: \n";
			menu_draw(tree);

		}
		else
		{
			std::cout << "Incorrect input!\n";
			PrintMenu();
		}

	} while (choise != "q");


	/*
	tree.Insert(10);
	std::cout << std::endl << std::endl;
	tree.Draw();

	tree.Delete(10);
	std::cout << std::endl << std::endl;
	tree.Draw();

	tree.Delete(32);
	std::cout << std::endl << std::endl;
	tree.Draw();*/

	//for (;;);

	return 0;
}

void PrintMenu()
{
	using namespace std;
	cout << "Funkcje: \n";
	cout << "i - insert key\n";
	cout << "r - remove key\n";
	cout << "d - draw tree\n";
	cout << "p - print tree\n";
	cout << "q - quit\n";
	cout << "h - help\n";
	cout << "g - generate random tree\n";
	cout << endl;
}

void menu_insert(AvlTree<int>& tree)
{
	std::cout << "Enter the key(number) to insert: ";
	int key = 0;
	std::cin >> key;
	if (std::cin.fail())
	{
		std::cout << "Not a number!";
		std::cin.clear();
		return;
	}
	if (tree.Insert(key))
	{
		std::cout << "The key has been added.\n";
	}
	else
	{
		std::cout << "Key already exist!\n";
	}
}

void menu_remove(AvlTree<int>& tree)
{
	std::cout << "Enter the key(number) to remove: ";
	int key = 0;
	std::cin >> key;
	if (std::cin.fail())
	{
		std::cout << "Not a number!";
		std::cin.clear();
		return;
	}
	if (tree.Delete(key) != nullptr)
	{
		std::cout << "The key has been removed.\n";
	}
	else
	{
		std::cout << "Key not found!\n";
	}
}

void menu_draw(const AvlTree<int>& tree)
{
	tree.Draw();
}

void menu_print(AvlTree<int>& tree)
{
	std::cout << "Select print method (1 - pre order, 2 - in order, 3 - post order): ";
	std::string method = "_";

	std::cin >> method;
	if (method == "1") { tree.print_preorder(); }
	else if (method == "2") { tree.print_inorder(); }
	else if (method == "3") { tree.print_postorder(); }
	else
	{
		std::cout << "Incorrect choise!\n";
		PrintMenu();
	}
}