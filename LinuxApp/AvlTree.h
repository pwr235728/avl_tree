﻿#pragma once
#include <memory>
#include <string>
#include <iostream>
#include <iomanip>

namespace shapes
{
	const unsigned char node = 254;
	const unsigned char top_r = 218;
	const unsigned char bottom_r = 192;
	const unsigned char middle = 195;
	const unsigned char vertical = 179;
	const unsigned char horizontal = 196;
};

// bf = hL - hR 

template<class TKey>
struct AvlNode
{
	std::shared_ptr<AvlNode<TKey>> parent_node;;
	std::shared_ptr<AvlNode<TKey>> left_node;
	std::shared_ptr<AvlNode<TKey>> right_node;

	TKey key;

	int balance_factor;

	AvlNode(TKey key);

	bool is_balanced() const;

	std::shared_ptr<AvlNode<TKey>> taller_child();
};

template <class TKey>
AvlNode<TKey>::AvlNode(TKey key): parent_node(nullptr), left_node(nullptr), right_node(nullptr), key(key),
                                  balance_factor(0)
{
}

template <class TKey>
bool AvlNode<TKey>::is_balanced() const
{
	return (balance_factor > -2 && balance_factor < 2);
}

template <class TKey>
std::shared_ptr<AvlNode<TKey>> AvlNode<TKey>::taller_child()
{
	if (balance_factor >= 0) return left_node;
	else return right_node;
}

template<class TKey>
class AvlTree
{

public:
	AvlTree()
		: root_node(nullptr)
	{
	}

	bool Insert(TKey key)
	{
		auto new_node = std::make_shared<AvlNode<TKey>>(key);
		auto tmp_node = root_node;

		if (tmp_node == nullptr)
		{
			root_node = new_node;
			return true;
		}

		// inserting node

		while (tmp_node != nullptr)
		{
			if (tmp_node->key == key) { return false; } // duplicate key
			if (tmp_node->key > key)
			{
				if (tmp_node->left_node == nullptr)
				{
					tmp_node->left_node = new_node;
					new_node->parent_node = tmp_node;
					tmp_node->balance_factor += 1;
					break;
				}
				tmp_node = tmp_node->left_node;
			}
			else // tmp_node->key < key
			{
				if (tmp_node->right_node == nullptr)
				{
					tmp_node->right_node = new_node;
					new_node->parent_node = tmp_node;
					tmp_node->balance_factor -= 1;
					break;
				}
				tmp_node = tmp_node->right_node;
			}
		}

		// posible b_f values here: -1, 0, 1
		// when 0 - exit, tree is balanced
		// when -1 or 1 tree height changed
		if (tmp_node->balance_factor == 0)
			return true;

		// tree height changed

		auto parent = tmp_node->parent_node;

		// go upward and find first unbalanced node
		while (parent != nullptr)
		{
			if (parent->balance_factor != 0)		// if b_f == 0, node is balanced. updata bf and continue search
				break;							// node unbalanced. leave and rebalance_after_delete

			if (parent->left_node == tmp_node) parent->balance_factor = 1;
			else parent->balance_factor = -1;

			tmp_node = parent;
			parent = parent->parent_node;
		}

		if (parent == nullptr) return true;

		if (parent->balance_factor == -1)
		{
			if (parent->left_node == tmp_node) parent->balance_factor = 0;	// after insertion node is balanced
			else if (tmp_node->balance_factor == 1) rotate_rl(parent);
			else rotate_rr(parent);
		}
		else
		{
			if (parent->right_node == tmp_node) parent->balance_factor = 0;	// after insertion node is balanced
			else if (tmp_node->balance_factor == -1) rotate_lr(parent);
			else rotate_ll(parent);
		}

		return true;
	}

	std::shared_ptr<AvlNode<TKey>> Delete(TKey key)
	{
		std::shared_ptr<AvlNode<TKey>> node = Find(key);
		return delete_node(node);
	}

	void rebalance_after_delete(std::shared_ptr<AvlNode<TKey>> action_node)
	{
		if (action_node == nullptr) return;

		if (!action_node->is_balanced())
		{
			std::shared_ptr<AvlNode<TKey>> x = action_node;
			std::shared_ptr<AvlNode<TKey>> y = x->taller_child();

			if (x->balance_factor == -2)
			{
				if (y->balance_factor <= 0)
				{
					rotate_rr(x);
				}
				else
				{
					rotate_rl(x);
				}
			}
			else if (x->balance_factor == 2)
			{
				if (y->balance_factor >= 0)
				{
					rotate_ll(x);
				}
				else
				{
					rotate_lr(x);
				}
			}
			action_node = action_node->parent_node;
			rebalance_after_delete(action_node);
		}
		else if (action_node->balance_factor == 0 && action_node->parent_node != nullptr)
		{
			auto parent = action_node->parent_node;

			if (parent->left_node == action_node) parent->balance_factor -= 1;
			else parent->balance_factor += 1;

			rebalance_after_delete(parent);
		}
	}


	void rebalance_after_delete2(std::shared_ptr<AvlNode<TKey>> action_node)
	{
		while (action_node != nullptr)
		{
			if (!action_node->is_balanced())
			{
				auto x = action_node;
				auto y = x->taller_child();

				if (x->balance_factor == -2)
				{
					if (x->left_node == y) y->balance_factor = 0;
					else if (y->balance_factor == 1) rotate_rl(x);
					else rotate_rr(x);
				}
				else if (x->balance_factor == 2)
				{
					if (x->right_node == y) x->balance_factor = 0;
					else if (y->balance_factor == -1) rotate_lr(x);
					else rotate_ll(x);
				}

			}
			else if (action_node->balance_factor == 0)
			{
				auto parent = action_node->parent_node;

				if (parent && parent->left_node == action_node) parent->balance_factor -= 1;
				else if (parent) parent->balance_factor += 1;
				if (parent && parent->balance_factor != 0)
					break;
				action_node = parent;
			}
			else { break; }

		}
		/*		while(action_node && action_node->balance_factor == 0)
		{
		auto parent = action_node->parent_node;

		if (parent && parent->left_node == action_node) parent->balance_factor -= 1;
		else if (parent) parent->balance_factor += -1;
		}*/
	}

	std::shared_ptr<AvlNode<TKey>> delete_node(std::shared_ptr<AvlNode<TKey>> node)
	{
		if (node == nullptr) return nullptr;

		if (node->left_node == nullptr && node->right_node == nullptr)
		{
			if (node == root_node)
			{
				root_node = nullptr;
				return node;
			}
			std::shared_ptr<AvlNode<TKey>> action_node = node->parent_node;

			if (action_node->left_node == node) {

				action_node->left_node = nullptr;
				action_node->balance_factor -= 1;
			}
			else
			{
				action_node->right_node = nullptr;
				action_node->balance_factor += 1;
			}
			node->parent_node = nullptr;
			rebalance_after_delete(action_node);
			return node;
		}
		else if (node->left_node == nullptr && node->right_node != nullptr)
		{
			if (node == root_node)
			{
				root_node = node->right_node;
				root_node->parent_node = nullptr;
				node->right_node = nullptr;
				return node;
			}

			std::shared_ptr<AvlNode<TKey>> action_node = node->parent_node;
			node->right_node->parent_node = node->parent_node;
			if (action_node->left_node == node)
			{
				action_node->left_node = node->right_node;
				action_node->balance_factor -= 1;
			}
			else
			{
				action_node->right_node = node->right_node;
				action_node->balance_factor += 1;
			}
			node->parent_node = nullptr;
			node->right_node = nullptr;
			rebalance_after_delete(action_node);
			return node;
		}
		else if (node->left_node != nullptr && node->right_node == nullptr)
		{
			if (node == root_node)
			{
				root_node = node->left_node;
				root_node->parent_node = nullptr;
				node->left_node = nullptr;
				return node;
			}

			std::shared_ptr<AvlNode<TKey>> action_node = node->parent_node;
			node->left_node->parent_node = node->parent_node;
			if (action_node->left_node == node)
			{
				action_node->left_node = node->left_node;
				action_node->balance_factor -= 1;
			}
			else
			{
				action_node->right_node = node->left_node;
				action_node->balance_factor += 1;
			}
			node->parent_node = nullptr;
			node->left_node = nullptr;
			rebalance_after_delete(action_node);
			return node;
		}
		else // 2 child nodes :( 
		{
			std::shared_ptr<AvlNode<TKey>> successor = left_most(node->right_node); // in-order successor
			TKey tmp = node->key;
			node->key = successor->key; // swap 
			successor->key = tmp;

			return delete_node(successor);
		}
	}

	std::shared_ptr<AvlNode<TKey>> Find(TKey key) const
	{
		auto node = root_node;

		while (node != nullptr && node->key != key)
		{
			if (node->key > key)
				node = node->left_node;
			else
				node = node->right_node;
		}
		return node;
	}
	void Draw() const
	{
		draw_node("", root_node);
	}

	void print_preorder() const
	{
		print_preorder(root_node);
	}

	void print_inorder() const
	{
		print_inorder(root_node);
	}

	void print_postorder() const
	{
		print_postorder(root_node);
	}
private:
	std::shared_ptr<AvlNode<TKey>> root_node;

	void print_preorder(std::shared_ptr<AvlNode<TKey>> node) const
	{
		if (node == nullptr) return;
		std::cout << node->key << " : ";
		print_preorder(node->left_node);
		print_preorder(node->right_node);
	}
	void print_inorder(std::shared_ptr<AvlNode<TKey>> node) const
	{
		if (node == nullptr) return;
		print_inorder(node->left_node);
		std::cout << node->key << " : ";		
		print_inorder(node->right_node);
	}
	void print_postorder(std::shared_ptr<AvlNode<TKey>> node) const
	{
		if (node == nullptr) return;		
		print_postorder(node->left_node);
		print_postorder(node->right_node);
		std::cout << node->key << " : ";
	}

	void fill(std::string route) const
	{
		if (route.length() > 0)
		{
			for (unsigned i = 0; i < (route.length() - 1); i++)
			{
				if (route[i] != route[i + 1])
				{
					std::cout << shapes::vertical << "    "; // "|    "
				}
				else
					std::cout << "     ";
			}

			if (route[route.length() - 1] == 'R')
				std::cout << shapes::top_r << shapes::horizontal << shapes::horizontal << shapes::horizontal << shapes::horizontal; // ".----"
			else
				std::cout << shapes::bottom_r << shapes::horizontal << shapes::horizontal << shapes::horizontal << shapes::horizontal; // "'----";
		}
	}
	void draw_node(const std::string route, std::shared_ptr<AvlNode<TKey>> v) const
	{
		if (v != nullptr)
		{
			draw_node(route + "R", v->right_node);
			fill(route);
			std::cout << "" << shapes::node << "[" << v->balance_factor << ": " << v->key << "]" << std::endl;
			draw_node(route + "L", v->left_node);
		}
	}

	std::shared_ptr<AvlNode<TKey>> left_most(std::shared_ptr<AvlNode<TKey>> node)
	{
		while (node->left_node != nullptr)
			node = node->left_node;

		return node;
	}

	void rotate_rr(std::shared_ptr<AvlNode<TKey>> node_a)
	{
		auto parent_a = node_a->parent_node;
		auto node_b = node_a->right_node;

		if ((node_a->right_node = node_b->left_node) != nullptr)
		{
			node_a->right_node->parent_node = node_a;
		}

		node_b->left_node = node_a;
		node_b->parent_node = parent_a;
		node_a->parent_node = node_b;

		if (parent_a == nullptr)
		{
			root_node = node_b;
		}
		else if (parent_a->left_node == node_a)
		{
			parent_a->left_node = node_b;
		}
		else
		{
			parent_a->right_node = node_b;
		}

		if (node_b->balance_factor == -1)
		{
			node_a->balance_factor = 0;
			node_b->balance_factor = 0;
		}
		else
		{
			node_a->balance_factor = -1;
			node_b->balance_factor = 1;
		}
	}
	void rotate_ll(std::shared_ptr<AvlNode<TKey>> node_a)
	{
		auto parent_a = node_a->parent_node;
		auto node_b = node_a->left_node;

		if ((node_a->left_node = node_b->right_node) != nullptr)
		{
			node_a->left_node->parent_node = node_a;
		}

		node_b->right_node = node_a;
		node_b->parent_node = parent_a;
		node_a->parent_node = node_b;

		if (parent_a == nullptr)
		{
			root_node = node_b;
		}
		else if (parent_a->left_node == node_a)
		{
			parent_a->left_node = node_b;
		}
		else
		{
			parent_a->right_node = node_b;
		}

		if (node_b->balance_factor == 1)
		{
			node_a->balance_factor = 0;
			node_b->balance_factor = 0;
		}
		else
		{
			node_a->balance_factor = 1;
			node_b->balance_factor = -1;
		}
	}
	void rotate_rl(std::shared_ptr<AvlNode<TKey>> node_a)
	{
		auto parent_a = node_a->parent_node;
		auto node_b = node_a->right_node;
		auto node_c = node_b->left_node;

		if ((node_b->left_node = node_c->right_node) != nullptr)
			node_b->left_node->parent_node = node_b;


		if ((node_a->right_node = node_c->left_node) != nullptr)
			node_a->right_node->parent_node = node_a;


		node_c->left_node = node_a;
		node_c->right_node = node_b;
		node_a->parent_node = node_c;
		node_b->parent_node = node_c;
		node_c->parent_node = parent_a;

		if (parent_a == nullptr)
			root_node = node_c;
		else if (parent_a->left_node == node_a)
			parent_a->left_node = node_c;
		else
			parent_a->right_node = node_c;

		if (node_c->balance_factor == -1)
			node_a->balance_factor = 1;
		else
			node_a->balance_factor = 0;

		if (node_c->balance_factor == 1)
			node_b->balance_factor = -1;
		else
			node_b->balance_factor = 0;

		node_c->balance_factor = 0;
	}
	void rotate_lr(std::shared_ptr<AvlNode<TKey>> node_a)
	{
		auto parent_a = node_a->parent_node;
		auto node_b = node_a->left_node;
		auto node_c = node_b->right_node;

		if ((node_b->right_node = node_c->left_node) != nullptr)
			node_b->right_node->parent_node = node_b;


		if ((node_a->left_node = node_c->right_node) != nullptr)
			node_a->left_node->parent_node = node_a;


		node_c->right_node = node_a;
		node_c->left_node = node_b;
		node_a->parent_node = node_c;
		node_b->parent_node = node_c;
		node_c->parent_node = parent_a;

		if (parent_a == nullptr)
			root_node = node_c;
		else if (parent_a->left_node == node_a)
			parent_a->left_node = node_c;
		else
			parent_a->right_node = node_c;

		if (node_c->balance_factor == 1)
			node_a->balance_factor = -1;
		else
			node_a->balance_factor = 0;

		if (node_c->balance_factor == -1)
			node_b->balance_factor = 1;
		else
			node_b->balance_factor = 0;

		node_c->balance_factor = 0;
	}
};
